package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;

import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.List;

public class CompanyMain {
    public static void main(String[] args) {
        Company company = new Company();

        Ceo luffy = new Ceo("Luffy", 200000.00);
        company.addEmployee(luffy);

        Cto zorro = new Cto("Zorro", 100000.00);
        company.addEmployee(zorro);

        BackendProgrammer franky = new BackendProgrammer("Franky", 20000.00);
        company.addEmployee(franky);

        BackendProgrammer usopp = new BackendProgrammer("Usopp", 20000.00);
        company.addEmployee(usopp);

        FrontendProgrammer nami = new FrontendProgrammer("Nami",30000.00);
        company.addEmployee(nami);

        FrontendProgrammer robin = new FrontendProgrammer("Robin", 30000.00);
        company.addEmployee(robin);

        UiUxDesigner sanji = new UiUxDesigner("sanji", 90000.00);
        company.addEmployee(sanji);

        NetworkExpert brook = new NetworkExpert("Brook", 50000.00);
        company.addEmployee(brook);

        SecurityExpert chopper = new SecurityExpert("Chopper", 70000.00);
        company.addEmployee(chopper);

        List<Employees> allEmployees = company.getAllEmployees();
        System.out.println("Company Employees:\n");
        for (Employees employee: allEmployees) {
            System.out.println(employee.getName() + " as a " + employee.getRole()
                + " with salary of " + employee.getSalary());
        }

        System.out.print("\nNet salary: ");
        System.out.println(company.getNetSalaries());
    }
}
