package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;

import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cucumber;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Tomato;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.TomatoSauce;

public class FoodMain {
    public static void main(String[] args) {
        Food thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        System.out.println(thickBunBurgerSpecial.getDescription()
                + " is ready, cost: " + thickBunBurgerSpecial.cost());

        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription()
                + ", cost: " + thickBunBurgerSpecial.cost());

        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription()
                + ", cost: " + thickBunBurgerSpecial.cost());

        thickBunBurgerSpecial = FillingDecorator.CUCUMBER.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription()
                + ", cost: " + thickBunBurgerSpecial.cost());

        thickBunBurgerSpecial = FillingDecorator.LETTUCE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription()
                + ", cost: " + thickBunBurgerSpecial.cost());

        thickBunBurgerSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription() 
                + ", cost: " + thickBunBurgerSpecial.cost() + "\n");

        Food thinBunBurgerVegetarian = BreadProducer.THIN_BUN.createBreadToBeFilled();
        System.out.println(thinBunBurgerVegetarian.getDescription()
                + " is ready, cost: " + thinBunBurgerVegetarian.cost());

        thinBunBurgerVegetarian = FillingDecorator.TOMATO.addFillingToBread(
                thinBunBurgerVegetarian);
        System.out.println(thinBunBurgerVegetarian.getDescription()
                + ", cost: " + thinBunBurgerVegetarian.cost());

        thinBunBurgerVegetarian = FillingDecorator.LETTUCE.addFillingToBread(
                thinBunBurgerVegetarian);
        System.out.println(thinBunBurgerVegetarian.getDescription()
                + ", cost: " + thinBunBurgerVegetarian.cost());

        thinBunBurgerVegetarian = FillingDecorator.CUCUMBER.addFillingToBread(
                thinBunBurgerVegetarian);
        System.out.println(thinBunBurgerVegetarian.getDescription()
                + ", cost: " + thinBunBurgerVegetarian.cost() + "\n");

        Food doubleBeefChickenDoubleSauceSandwich =
                BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription()
                + " is ready, cost: " + doubleBeefChickenDoubleSauceSandwich.cost());

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.BEEF_MEAT.addFillingToBread(
                doubleBeefChickenDoubleSauceSandwich);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription()
                + ", cost: " + doubleBeefChickenDoubleSauceSandwich.cost());

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                doubleBeefChickenDoubleSauceSandwich);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription()
                + ", cost: " + doubleBeefChickenDoubleSauceSandwich.cost());

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.CHILI_SAUCE.addFillingToBread(
                doubleBeefChickenDoubleSauceSandwich);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription()
                + ", cost: " + doubleBeefChickenDoubleSauceSandwich.cost());

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription()
                + ", cost: " + doubleBeefChickenDoubleSauceSandwich.cost() + "\n");

        Food noCrustAllFillingSandwich = BreadProducer.NO_CRUST_SANDWICH
                .createBreadToBeFilled();
        System.out.println(noCrustAllFillingSandwich.getDescription()
                + " is ready, cost: " + noCrustAllFillingSandwich.cost());

        noCrustAllFillingSandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription()
                + ", cost: " + noCrustAllFillingSandwich.cost());

        noCrustAllFillingSandwich = FillingDecorator.CHEESE.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription()
                + ", cost: " + noCrustAllFillingSandwich.cost());

        noCrustAllFillingSandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription()
                + ", cost: " + noCrustAllFillingSandwich.cost());

        noCrustAllFillingSandwich = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription()
                + ", cost: " + noCrustAllFillingSandwich.cost());

        noCrustAllFillingSandwich = FillingDecorator.CUCUMBER.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription()
                + ", cost: " + noCrustAllFillingSandwich.cost());

        noCrustAllFillingSandwich = FillingDecorator.LETTUCE.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription()
                + ", cost: " + noCrustAllFillingSandwich.cost());

        noCrustAllFillingSandwich = FillingDecorator.TOMATO.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription()
                + ", cost: " + noCrustAllFillingSandwich.cost());

        noCrustAllFillingSandwich = FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription()
                + ", cost: " + noCrustAllFillingSandwich.cost());
    }
}
