package tallycounter;

public class SynchronizedTallyCounter extends TallyCounter {

    public synchronized void increment() {
        counter++;
    }

    public synchronized void decrement() {
        counter--;
    }

    public synchronized int value() {
        return counter;
    }
}
