package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class HardClamsTest {
    private HardClams hardClams;

    @Before
    public void setUp() {
        hardClams = new HardClams();
    }

    @Test
    public void testToString() {
        assertEquals("Hard Clams from Northern Quahog", hardClams.toString());
    }
}
