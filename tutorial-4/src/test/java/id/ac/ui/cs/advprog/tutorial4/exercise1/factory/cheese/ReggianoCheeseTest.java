package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ReggianoCheeseTest {
    private ReggianoCheese regiCheese;

    @Before
    public void setUp() {
        regiCheese = new ReggianoCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Reggiano Cheese", regiCheese.toString());
    }
}
