package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CheesePizzaTest {
    final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private Pizza cheesePizza;

    @Before
    public void setUp() {
        cheesePizza = new CheesePizza(new NewYorkPizzaIngredientFactory());
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void testPrepare() {
        cheesePizza.setName("Cheese Pizza Tester");
        cheesePizza.prepare();
        assertEquals("Preparing Cheese Pizza Tester" 
            + System.getProperty("line.separator"), outContent.toString());
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }
}
