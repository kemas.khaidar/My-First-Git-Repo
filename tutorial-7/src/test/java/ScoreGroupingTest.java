import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    Map<String, Integer> scores = new HashMap<>();
    ScoreGrouping passer;

    @Before
    public void setUp() {
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
        passer = new ScoreGrouping();
    }

    @Test
    public void testGrouping() {
        assertEquals("{11=[Charlie, Foxtrot], 12=[Alice], 15=[Emi, Bob, Delta]}",
            ScoreGrouping.groupByScores(scores).toString());
    }

    @Test
    public void testMain() {
        System.out.println("main");
        final InputStream original = System.in;
        ScoreGrouping.main(null);
        System.setIn(original);
    }
}
