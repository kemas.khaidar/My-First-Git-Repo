package applicant;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

import java.io.InputStream;
import java.util.function.Predicate;

import org.junit.Test;

public class ApplicantTest {
    private Applicant applicant = new Applicant();
    private Predicate<Applicant> qualifiedEvaluator = Applicant::isCredible;
    private Predicate<Applicant> creditEvaluator = applicant1 -> applicant1.getCreditScore() > 600;
    private Predicate<Applicant> employmentEvaluator = applicant1
        -> applicant1.getEmploymentYears() > 0;
    private Predicate<Applicant> crimeCheck = applicant1 -> !applicant1.hasCriminalRecord();

    @Test
    public void testEvaluate() {
        boolean result = Applicant.evaluate(applicant, qualifiedEvaluator);
        assertTrue(result);
        boolean result2 = Applicant.evaluate(applicant, creditEvaluator);
        assertTrue(result2);
        boolean result3 = Applicant.evaluate(applicant, employmentEvaluator);
        assertTrue(result3);
        boolean result4 = Applicant.evaluate(applicant, crimeCheck);
        assertFalse(result4);
    }

    @Test
    public void testCreditScore() {
        assertEquals(700, applicant.getCreditScore());
    }

    @Test
    public void testEmploymentYears() {
        assertEquals(10, applicant.getEmploymentYears());
    }

    @Test
    public void testCriminalRecord() {
        assertTrue(applicant.hasCriminalRecord());
    }

    @Test
    public void testMain() {
        System.out.println("main");
        final InputStream original = System.in;
        Applicant.main(null);
        System.setIn(original);
    }
}
